<?php declare(strict_types = 1);

namespace WHR;

use Exception;

/**
 * Get event source from request header
 *
 * @return string|null
 */
function get_source_from_header(): ?string {
	if ( isset( $_SERVER['HTTP_X_GITHUB_EVENT'] ) ) {
		return 'github';
	}

	if ( isset( $_SERVER['HTTP_X_GITLAB_EVENT'] ) ) {
		return 'gitlab';
	}

	return null;
}

/**
 * Get event data
 *
 * @return array<mixed>|null
 */
function get_data(): ?array {
	$json = file_get_contents( 'php://input' );

	if ( empty( $json ) ) {
		throw new Exception( 'Empty data.', 400 );
	}

	$data = json_decode( $json, true );

	if ( json_last_error() ) {
		throw new Exception( json_last_error_msg(), 400 );
	}

	return $data;
}

/**
 * Get source repo headers map
 *
 * @return array<array<string>>
 */
function get_source_repo_headers_map(): array {
	return [
		'github' => [
			'event_key' => 'HTTP_X_GITHUB_EVENT',
			'token_key' => 'HTTP_X_HUB_SIGNATURE',
		],
		'gitlab' => [
			'event_key' => 'HTTP_X_GITLAB_EVENT',
			'token_key' => 'HTTP_X_GITLAB_TOKEN',
		],
	];
}

/**
 * Get project configuration
 *
 * @param string                      $source   Webhook source.
 * @param array<mixed>                $data     Event data.
 * @param array<string,array<string>> $projects All project configurations.
 *
 * @return array<string,array<string>>
 */
function get_project_config( string $source, array $data, array $projects ): array {
	$found_configs = array_filter(
		$projects,
		static function ( array $config ) use ( $source, $data ): bool {
			if ( ! isset( $config['name'] ) ) {
				return false;
			}

			if ( $source !== $config['source'] ) {
				return false;
			}

			if ( $source === 'github' ) {
				if ( ! isset( $data['repository']['full_name'] ) ) {
					return false;
				}

				return $data['repository']['full_name'] === $config['name'];
			}

			// GitLab.
			if ( ! isset( $data['project']['path_with_namespace'] ) ) {
				return false;
			}

			return $data['project']['path_with_namespace'] === $config['name'];
		},
	);

	if ( empty( $found_configs ) ) {
		return [];
	}

	$found_configs = array_values( $found_configs );

	return $found_configs[0];
}

/**
 * Get configuration
 *
 * @param string        $source Webhook source.
 * @param array<mixed>  $data   Event data.
 *
 * @return array<string,array<string>>|null
 */
function get_config( string $source, array $data ): ?array {
	$config_file = dirname( dirname( __DIR__ ) ) . '/config.json';

	if ( ! file_exists( $config_file ) ) {
		throw new Exception( 'Config file does not exist.', 500 );
	}

	$raw = file_get_contents( $config_file );
	$config = json_decode( $raw, true );

	if ( json_last_error() ) {
		throw new Exception( json_last_error_msg(), 500 );
	}

	if ( ! isset( $config['projects'] ) ) {
		throw new Exception( 'Config error: `projects` is not set.', 500 );
	}

	if ( ! is_array( $config['projects'] ) || empty( $config['projects'] ) ) {
		throw new Exception( 'Config error: Malformed `projects`.', 500 );
	}

	$project_config = get_project_config( $source, $data, $config['projects'] );

	if ( empty( $project_config ) ) {
		throw new Exception( 'Config error: No such project.', 500 );
	}

	if ( empty( $project_config['token'] ) ) {
		throw new Exception( 'Config error: `token` is not set.', 500 );
	}

	if ( empty( $project_config['events'] ) || ! is_array( $project_config['events'] ) ) {
		throw new Exception( 'Config error: `events` is not set or malformed.', 500 );
	}

	return $project_config;
}

/**
 * Is GitHub token valid?
 *
 * @param string $config_token   Token as set in configuration file.
 * @param string $received_token Token received in the request header.
 *
 * @return bool
 */
function is_github_token_valid( string $config_token, string $received_token ): bool {
	$data = file_get_contents( 'php://input' );
	[ $algo, $gh_token ] = explode( '=', $received_token, 2 );

	$hash = hash_hmac( $algo, $data, $config_token );

	return $gh_token === $hash;
}

/**
 * Is token valid?
 *
 * @param string $source Repository source (github, gitlab).
 * @param string $token  Token as set in configuration file.
 *
 * @return bool
 */
function is_token_valid( string $source, string $token ): bool {
	$sources_map = get_source_repo_headers_map();

	if ( ! isset( $sources_map[ $source ] ) ) {
		return false;
	}

	$token_key = $sources_map[ $source ]['token_key'];

	if ( empty( $_SERVER[ $token_key ] ) ) {
		return false;
	}

	$received_token = $_SERVER[ $token_key ];

	if ( $source === 'github' ) {
		return is_github_token_valid( $token, $received_token );
	}

	return $received_token === $token;
}

/**
 * Get gitlab event
 *
 * @param string $event_header Event name as received in the request header.
 *
 * @return string|null
 */
function get_gitlab_event( string $event_header ): ?string {
	$events = [
		'Issue Hook'         => 'issue',
		'Job Hook'           => 'build',
		'Merge Request Hook' => 'merge_request',
		'Note Hook'          => 'note',
		'Pipeline Hook'      => 'pipeline',
		'Push Hook'          => 'push',
		'Tag Push Hook'      => 'tag_push',
		'Wiki Page Hook'     => 'wiki_page',
	];

	return $events[ $event_header ] ?? null;
}

/**
 * Get event from request header
 *
 * @param string $source Repository source (github, gitlab).
 *
 * @return string|null
 */
function get_event( string $source ): ?string {
	$sources_map = get_source_repo_headers_map();

	if ( ! isset( $sources_map[ $source ] ) ) {
		throw new Exception( 'Invalid source.', 400 );
	}

	$header_key = $sources_map[ $source ]['event_key'];

	if ( empty( $_SERVER[ $header_key ] ) ) {
		throw new Exception( 'Could not find webhook event in the request headers.', 400 );
	}

	switch ( $source ) {
		case 'gitlab':
			$event = get_gitlab_event( $_SERVER[ $header_key ] );
			break;

		default:
			$event = preg_replace( '/[^a-z_]/', '', $_SERVER[ $header_key ] );
	}

	if ( empty( $event ) ) {
		throw new Exception( 'Invalid event.', 400 );
	}

	return $event;
}

/**
 * Write event data
 *
 * @param string       $event   Event name.
 * @param string       $log_dir Log directory.
 * @param array<mixed> $data    Event data.
 */
function write_data( string $event, string $log_dir, array $data ): void {
	$dest_dir = sprintf( '%s/%s', rtrim( $log_dir, '/' ), $event );

	if ( ! file_exists( $dest_dir ) ) {
		$created = mkdir( $dest_dir, 0755, true );

		if ( ! $created ) {
			throw new Exception( 'Failed to create event log directory.', 500 );
		}
	}

	$dest_file = sprintf( '%s/%s.json', $dest_dir, gmdate( 'c' ) );
	$content   = json_encode( $data, JSON_PRETTY_PRINT );
	$handle    = fopen( $dest_file, 'w+' );

	if ( ! $handle ) {
		throw new Exception( 'Failed to create event log file.', 500 );
	}

	$written = fwrite( $handle, $content );

	if ( ! $written ) {
		throw new Exception( 'Failed to write event log file.', 500 );
	}

	fclose( $handle );
}
