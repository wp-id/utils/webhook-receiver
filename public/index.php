<?php declare(strict_types = 1);

namespace WHR;

use Exception;
use Throwable;

require_once __DIR__ . '/inc/namespace.php';

header( 'Content-Type: text/plain' );

try {
	$source = get_source_from_header();

	if ( empty( $source ) ) {
		throw new Exception( 'Source not found.', 400 );
	}

	$data   = get_data();
	$config = get_config( $source, $data );

	if ( ! is_token_valid( $source, $config['token'] ) ) {
		throw new Exception( '*** ACCESS DENIED ***', 403 );
	}

	$event = get_event( $source );

	if ( ! in_array( $event, $config['events'], true ) ) {
		throw new Exception( 'Not implemented.', 501 );
	}

	write_data( $event, $config['log_dir'], $data );
} catch ( Throwable $e ) {
	http_response_code( $e->getCode() );
	exit( $e->getMessage() );
}

http_response_code( 200 );
echo 'Success!';
