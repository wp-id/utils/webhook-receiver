# Webhook Receiver

A generic webhook receiver that does one thing: write a file that contains event data to specified directory.

This receiver supports [GitHub](https://developer.github.com/webhooks/) and [GitLab](https://gitlab.com/help/user/project/integrations/webhooks) webhooks.

## Requirements
* PHP 7.1+
* SSL-enabled web server

## Installation

1. Copy the content of `public/` to your server's document root directory.
1. Create a `config.json` file one directory up of your server's document root directory (see [sample config](config.sample.json)).
    * Make sure the directory used as `log_dir` in the config is writable by the server.

## How It Works
Upon receiving a `POST` request containing valid headers, this script will check the config for the appropriate project. When it found one, it'll create a JSON file named after the current time (ISO 8601) containing the event data. It is then up to you what to do with the newly created file and the data it contains.

For example, when a `build` event is received and the `log_dir` in config is set to `/var/webhooks/my-project`, a file named `/var/webhooks/my-project/2019-09-21T14:40:55+00:00.json` will be created.

This receiver was designed to work with completely separated file watcher service in mind. The file watcher would watch for the changes made to the log directories and run an action whenever a new file is created.

For an example of a popular watcher, take a look at [Watchman](https://facebook.github.io/watchman/).

## Docker
Please take a look at the [example compose](https://gitlab.com/wp-id/docker/webhook-receiver-compose).
