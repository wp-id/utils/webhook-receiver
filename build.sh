#!/bin/sh

# See https://docs.gitlab.com/ee/ci/variables/

set -e

docker login -u "${DOCKER_USER}" -p "${DOCKER_PASSWORD}" $DOCKER_REGISTRY

docker pull $DOCKER_IMAGE_NAME || true

if [ $CI_COMMIT_REF_NAME = "master" ]; then
	NEW_IMAGE_NAME="${DOCKER_IMAGE_NAME}:latest"
	docker build --cache-from $DOCKER_IMAGE_NAME -t $NEW_IMAGE_NAME .
else
	NEW_IMAGE_NAME="${DOCKER_IMAGE_NAME}:${CI_COMMIT_TAG}"
	docker tag "${DOCKER_IMAGE_NAME}:latest" $NEW_IMAGE_NAME
fi

docker push $NEW_IMAGE_NAME
