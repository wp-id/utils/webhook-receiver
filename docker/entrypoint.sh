#!/bin/sh

# Terminate on errors.
set -e

cat << "EOF"

Webhook Receiver. Brought to you with 💜 by:

_ _ ____   ____  ____________
| | ||__]__||  \ |  ||__/| __
|_|_||     ||__/.|__||  \|__]

https://wp-id.org

EOF

# Stolen from https://hub.docker.com/u/binhex/
mkdir -p /logs

# Set www-data's user ID.
if [ ! -z "${USER_ID}" ]; then
    echo "[info] www-data user ID set to ${USER_ID}."
    usermod -o -u "${USER_ID}" www-data &>/dev/null
fi

# Set www-data's group ID.
if [ ! -z "${GROUP_ID}" ]; then
    echo "[info] www-data group ID set to ${GROUP_ID}."
    groupmod -o -g "${GROUP_ID}" www-data &>/dev/null
fi

chown -R www-data:www-data /webhook-logs

exec "$@"
