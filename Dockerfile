FROM php:7.3-fpm-alpine

RUN apk update \
    && apk add shadow \
    && rm -fr /var/cache/apk/*

VOLUME /var/www/html /webhook-logs

COPY public /var/www/html/
COPY docker/php-fpm.d /usr/local/etc/php-fpm.d/
COPY docker/entrypoint.sh /entrypoint.sh

ENTRYPOINT [ "/entrypoint.sh" ]

CMD [ "php-fpm" ]
